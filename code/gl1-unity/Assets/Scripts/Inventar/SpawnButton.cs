﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace CrazySlide
{
    [RequireComponent(typeof(SpawnSelectionBarSegment))]
    [RequireComponent(typeof(VRTK_InteractableObject))]
    public class SpawnButton : MonoBehaviour
    {
        private VRTK_InteractableObject vrtkIO = null;
        private SpawnSelectionBarSegment ssbs;

        void Start()
        {
            ssbs = GetComponent<SpawnSelectionBarSegment>();
            vrtkIO = GetComponent<VRTK_InteractableObject>();
            SetListener();
        }

        private void SendObjectToSpawner()
        {
            ssbs.spawnArea.ObjectToSpawn(ssbs.spawnElement);
        }

        #region controller listener setup
        private void SetListener()
        {
            vrtkIO.InteractableObjectUsed += InteractableUse;
        }

        private void InteractableUse(object sender, InteractableObjectEventArgs e)
        {
            VRTK_InteractableObject grabbedInteractableObject = sender as VRTK_InteractableObject;
            SendObjectToSpawner();
        }
        #endregion controller listener setup
    }
}
