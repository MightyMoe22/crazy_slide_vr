﻿using UnityEngine;
using VRTK;

namespace CrazySlide
{
    [RequireComponent(typeof(PlayAudioOnInteraction))]
    public class Trashcan : MonoBehaviour
    {

        public string tagForDelete = null;
        public PlayerInventory inventory;

        GameObject objectToDelet = null;

        private VRTK_InteractableObject vrtkIO = null;
        private int costRefund = 0;

        private void OnEnable()
        {
            inventory = GameObject.Find("PlayerInventar").GetComponent<PlayerInventory>();
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals("SlidePart") || other.gameObject.tag.Equals("Construction"))
            {
                ElementOrConstruction(other.gameObject);
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag.Equals(tagForDelete))
            {
                RemoveListener();
                objectToDelet = null;
                vrtkIO = null;
            }
        }

        private void ElementOrConstruction(GameObject gameObject)
        {
            if (gameObject.GetComponent<SnapperComs>().elementGrabbed)
            {
                Debug.Log("Its an Element");
                objectToDelet = gameObject;
                costRefund = gameObject.GetComponent<ElementPrice>().elementPrice;
                SetListener();
            }
            else if (gameObject.GetComponent<SnapperComs>().constrGrabbed)
            {
                Debug.Log("Its a Construction");
                objectToDelet = gameObject.GetComponent<SnapperComs>().construction;
                costRefund = CalculateRefund(gameObject.GetComponent<SnapperComs>().construction);
                SetListener();
            }
            else
            {
                objectToDelet = null;
            }
        }

        private int CalculateRefund(GameObject construction)
        {
            int refund = 0;
            foreach (GameObject go in construction.GetComponent<Construction>().elementList)
            {
                refund += go.GetComponent<ElementPrice>().elementPrice;
            }
            return refund;
        }

        private void SetListener()
        {
            vrtkIO = objectToDelet.GetComponent<VRTK_InteractableObject>();
            //vrtkIO.InteractableObjectGrabbed += InteractableGrabbed;
            vrtkIO.InteractableObjectUngrabbed += InteractableUngrabbed;
        }

        private void RemoveListener()
        {
            //vrtkIO.InteractableObjectGrabbed -= InteractableGrabbed;
            vrtkIO.InteractableObjectUngrabbed -= InteractableUngrabbed;
        }

        private void InteractableUngrabbed(object sender, InteractableObjectEventArgs e)
        {
            GetComponent<PlayAudioOnInteraction>().PlaySound();
            VRTK_InteractableObject releasedInteractableObject = sender as VRTK_InteractableObject;
            Destroy(objectToDelet);
            inventory.UpdateCredits(costRefund);
            costRefund = 0;
        }
    }
}
