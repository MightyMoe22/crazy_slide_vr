﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace CrazySlide
{
    public class Spawner : MonoBehaviour
    {
        public PlayerInventory inventory;
        public Transform spawnPosition;
        GameObject objectInSpawn = null;

        bool spawnOccupied = false;
        int spawnedObjectPrice = 0;

        void Update()
        {
            if (objectInSpawn != null && (objectInSpawn.GetComponent<VRTK_InteractableObject>() != null && objectInSpawn.GetComponent<VRTK_InteractableObject>().IsGrabbed()))
            {
                objectInSpawn = null;
                spawnOccupied = false;
            }
        }
        private void OnEnable()
        {
            if (objectInSpawn != null && objectInSpawn.tag.Equals("Trashcan"))
                DespawnObject();
            else
            {
                objectInSpawn = null;
                spawnOccupied = false;
            }
        }

        public void ObjectToSpawn(GameObject objectToSpawn)
        {
            int cost = objectToSpawn.GetComponent<ElementPrice>().elementPrice;
            if (inventory.infiniteCapital || (inventory.Credits - cost) >= 0)
            {
                if (spawnOccupied)
                {
                    DespawnObject();
                }
                if (objectToSpawn != null)
                {
                    SpawnObject(objectToSpawn, cost);
                }
            }
        }

        private void SpawnObject(GameObject objectToSpawn, int cost)
        {
            objectInSpawn = Instantiate(objectToSpawn);
            objectInSpawn.transform.position = spawnPosition.position;
            objectToSpawn.transform.rotation = spawnPosition.rotation;
            spawnOccupied = true;
            spawnedObjectPrice = cost;
            inventory.UpdateCredits(-cost);
        }

        private void DespawnObject()
        {
            Destroy(objectInSpawn);
            objectInSpawn = null;
            spawnOccupied = false;
            inventory.UpdateCredits(spawnedObjectPrice);
            spawnedObjectPrice = 0;
        }
    }
}
