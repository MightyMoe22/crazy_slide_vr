﻿using UnityEngine;

namespace CrazySlide
{
    public class Belt : MonoBehaviour
    {
        public Transform playerToFollow;
        public Vector3 positionRelativToPlayer;
        void Update()
        {
            transform.position = playerToFollow.position + positionRelativToPlayer;
            transform.rotation = Quaternion.Euler(0, playerToFollow.rotation.eulerAngles.y, 0);
        }
    }
}
