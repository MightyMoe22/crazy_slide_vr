﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK;

namespace CrazySlide
{
    [RequireComponent(typeof(PlayAudioOnInteraction))]
    public class PlayerInventory : MonoBehaviour
    {
        public enum MainHand { left_handed, right_handed }

        [Tooltip("The credits which can be spend for building elements in this level")]
        public int playerCapital = 10000;
        public bool infiniteCapital = false;

        public MainHand mainHand = MainHand.right_handed;
        public SpawnSelectionBar spawnSelectionBar;
        public VRTK_ControllerEvents leftControllerEvents;
        public VRTK_ControllerEvents rightControllerEvents;
        public GameObject leftHand;
        public GameObject rightHand;
        private int credits = 0;
        private Text creditsDisplay;
        private bool creditsReset = true;

        public int Credits
        {
            get { return credits; }
            private set { credits = value; }
        }
        
        void Start()
        {
            SetUpControllerListener();
            UpdateCredits(playerCapital);
        }

        private void OnEnable()
        {
            SetUpControllerListener();
            UpdateCredits(0);
        }

        private void OnDisable()
        {
            ResetControllerListener();
        }

        public void UpdateCredits(int amount)
        {
            Credits += amount;
            if (!infiniteCapital) creditsDisplay.text = Credits.ToString() + "$";
            else creditsDisplay.text = "Infinite";
        }

        #region controller listener setup
        private void SetUpControllerListener()
        {
            if (mainHand == MainHand.right_handed)
            {
                leftControllerEvents.TouchpadTouchStart += TouchpadTouchStart;
                leftControllerEvents.TouchpadTouchEnd += TouchpadTouchEnd;
                creditsDisplay = spawnSelectionBar.SetUpSpawnSelectionBar(mainHand, leftHand.transform, this);
            }
            else
            {
                rightControllerEvents.TouchpadTouchStart += TouchpadTouchStart;
                rightControllerEvents.TouchpadTouchEnd += TouchpadTouchEnd;
                creditsDisplay = spawnSelectionBar.SetUpSpawnSelectionBar(mainHand, rightHand.transform, this);
            }
        }

        private void ResetControllerListener()
        {
            if (mainHand == MainHand.right_handed)
            {
                leftControllerEvents.TouchpadTouchStart -= TouchpadTouchStart;
                leftControllerEvents.TouchpadTouchEnd -= TouchpadTouchEnd;
                //creditsDisplay = spawnSelectionBar.SetUpSpawnSelectionBar(mainHand, leftHand.transform, this);
            }
            else
            {
                rightControllerEvents.TouchpadTouchStart -= TouchpadTouchStart;
                rightControllerEvents.TouchpadTouchEnd -= TouchpadTouchEnd;
                //creditsDisplay = spawnSelectionBar.SetUpSpawnSelectionBar(mainHand, rightHand.transform, this);
            }
        }

        private void TouchpadTouchStart(object sender, ControllerInteractionEventArgs e)
        {
            spawnSelectionBar.ShowSelectionBar();
            GetComponent<PlayAudioOnInteraction>().PlaySound();
        }

        private void TouchpadTouchEnd(object sender, ControllerInteractionEventArgs e)
        {
            spawnSelectionBar.HideSelectionBar();
            GetComponent<PlayAudioOnInteraction>().PlaySound();
        }

        #endregion controller events
    }
}
