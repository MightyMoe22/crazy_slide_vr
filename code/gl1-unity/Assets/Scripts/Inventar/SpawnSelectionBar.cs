﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CrazySlide
{
    public class SpawnSelectionBar : MonoBehaviour
    {
        public Transform barOrigin;

        private GameObject bar;
        private float handOffSet = 0.5f;
        private Transform hand;

        public Text SetUpSpawnSelectionBar(PlayerInventory.MainHand mainHand, Transform handTransform, PlayerInventory inventory)
        {
            Text temp;
            hand = handTransform;
            StartCoroutine(FollowHand());

            if (mainHand == PlayerInventory.MainHand.right_handed)
            {
                bar = Instantiate(Resources.Load("Prefab/Inventory/LeftHandedInventoryBar")) as GameObject;
                bar.transform.Find("Spawnlocation").gameObject.GetComponent<Spawner>().inventory = inventory;
                bar.transform.SetParent(barOrigin);
                bar.transform.localPosition = new Vector3(handOffSet, 0, 0);
                bar.transform.localRotation = transform.rotation;
                temp = bar.transform.Find("CreditsDisplay/Canvas/Credits Amount").gameObject.GetComponent<Text>();
            }
            else
            {
                bar = Instantiate(Resources.Load("Prefab/Inventory/RightHandedInventoryBar")) as GameObject;
                bar.transform.Find("Spawnlocation").gameObject.GetComponent<Spawner>().inventory = inventory;
                bar.transform.SetParent(barOrigin);
                bar.transform.localPosition = new Vector3(-handOffSet, 0, 0);
                bar.transform.localRotation = transform.rotation;
                temp = bar.transform.Find("CreditsDisplay/Canvas/Credits Amount").gameObject.GetComponent<Text>();
            }
            bar.gameObject.SetActive(false);
            return temp;
        }


        public void ShowSelectionBar()
        {
            bar.gameObject.SetActive(true);
        }

        public void HideSelectionBar()
        {
            bar.gameObject.SetActive(false);
        }

        IEnumerator FollowHand()
        {
            while (true)
            {
                barOrigin.transform.position = hand.position;
                barOrigin.transform.rotation = hand.rotation;
                yield return null;
            }
        }
    }
}
