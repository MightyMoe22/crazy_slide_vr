﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace CrazySlide
{
    [RequireComponent(typeof(ChangeColorOnInteraction))]
    public class SpawnSelectionBarSegment : MonoBehaviour
    {
        public Transform displayCenter;
        public GameObject tile;
        public int elementCost = 1000;
        public GameObject spawnElement;
        public GameObject dispalyElement;

        [Tooltip("A float value which defines the x,y,z axis scale")]
        public float elementScale = 0.5f;
        public Spawner spawnArea = null;
        public bool enableAnimation = true;
        [Tooltip("If you want to use the displayElement directly with out instanciation")]
        public bool noInstantiationForDisplayElement = false;

        private GameObject displayObject = null;
        private float turnSpeed = 20f;
        private float scalingFactor = 0.2f;
        private float scalingSpeed = 0.25f;
        private bool isSetUp = false;
        private ChangeColorOnInteraction changeColor;

        void Start()
        {
            changeColor = GetComponent<ChangeColorOnInteraction>();
            SetUpSegment();
            SetSegmentActivitie(true);
        }

        public void SetUpSegment()
        {
            displayObject = noInstantiationForDisplayElement ? dispalyElement : Instantiate(dispalyElement);
            changeColor.SetUpChangeColor(displayObject);
            if (enableAnimation) GetDisplayReady();
            isSetUp = true;
        }

        private void OnEnable()
        {
            if (dispalyElement != null && isSetUp)
            {
                SetSegmentActivitie(true);
            }
        }

        private void OnDisable()
        {
            SetSegmentActivitie(false);
        }

        public void SetSegmentActivitie(bool active)
        {
            if (enableAnimation)
            {
                if (active)
                    StartCoroutine(DisplayAnimation());
                else
                    StopCoroutine(DisplayAnimation());
            }
        }

        private void GetDisplayReady()
        {
            displayObject.transform.SetParent(displayCenter);
            displayObject.transform.localPosition = new Vector3(0, 0, 0);
            displayObject.transform.localRotation = displayCenter.transform.localRotation;
            displayObject.transform.localScale = new Vector3(elementScale, elementScale, elementScale);
        }

        private void RotateDisplay()
        {
            displayObject.transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime);
        }

        private void PulseDisplay()
        {
            float currentScale = displayObject.transform.localScale.x;
            float scaleModifier = 1 + Mathf.PingPong(Time.time * scalingSpeed, scalingFactor);
            float nextScale = elementScale * scaleModifier;

            displayObject.transform.localScale = new Vector3(nextScale, nextScale, nextScale);
        }

        IEnumerator DisplayAnimation()
        {
            while (true)
            {
                RotateDisplay();
                PulseDisplay();
                yield return null;
            }
        }
    }
}
