﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace CrazySlide
{
    public class Snapper : MonoBehaviour
    {
        [Tooltip("Object Tag the Snapper should connect to")]
        public string compareTag = "SnapConnector";

        public static int snapperID;

        //[HideInInspector]
        public GameObject construction = null;

        GameObject pendingConstr;

        VRTK_InteractableObject parentObjIO = null;
        VRTK_InteractableObject constrObjIO = null;
        VRTK_InteractableObject oldConstrObjIO = null;

        GameObject targetSnapper = null;
        GameObject connection = null;

        SnapperComs snapComs = null;

        bool inConnection = false;
        bool parentGrabbed = false;
        bool constrGrabbed = false;
        bool updateConstr = false;
        bool phantomInPlace = false;
        bool createdConnection = true;

        void Start()
        {
            SetVRTKListener();
        }

        void Update()
        {
            Renderer rend = GetComponent<Renderer>();
            rend.material.color = Color.green;
            if (!inConnection)
            {
                rend.material.color = Color.green;
            }
            else
            {
                rend.material.color = Color.red;
            }
        }

        /// <summary>
        /// Used form the snapperComs script to tell with whom to communicate
        /// </summary>
        /// <param name="coms">SnapperComs object to communicate with</param>
        public void SetSnapperComs(GameObject coms)
        {
            snapComs = coms.GetComponent<SnapperComs>();
        }

        /// <summary>
        /// Used from the snapperComs script to tell the snapper which construction it's part of
        /// </summary>
        /// <param name="constr">Construction which this snapper should be part of</param>
        public void SetConstruction(GameObject constr)
        {
            if (constr != null && constr.GetComponent<Construction>().isOrigin && constrObjIO != null)
            {
                oldConstrObjIO = constrObjIO;
                SetConstrListener(oldConstrObjIO);
                updateConstr = true;
            }
            if (construction != null)
            {
                RemoveConstrListener(constrObjIO);
                constrObjIO = null;
            }
            construction = constr;
            if (constr != null)
            {
                constrObjIO = construction.GetComponent<VRTK_InteractableObject>();
                SetConstrListener(constrObjIO);
            }
        }

        /// <summary>
        /// Used from the connection script to tell the snapper which connection it's part of
        /// </summary>
        /// <param name="connec"></param>
        public void SetConnection(GameObject connec)
        {
            connection = connec;
        }

        public void SetInConnection(bool status)
        {
            inConnection = status;
            if (status)
            {
                GetComponent<Collider>().enabled = false;
                createdConnection = false;
            }
            else
            {
                GetComponent<Collider>().enabled = true;
                createdConnection = true;
            }
        }

        /// <summary>
        /// Is called when Trigger enters
        /// </summary>
        /// <param name="collider">Collider of the Trigger that enters</param>
        /*private void OnTriggerEnter(Collider collider)
        {
            if (CheckValidTag(collider.gameObject) && (parentGrabbed || constrGrabbed) && !inConnection)
            {
                targetSnapper = collider.gameObject;
                //StartPhantom();
                CreateConnectionObj();
            }
        }*/

        private void OnTriggerStay(Collider collider)
        {
            if (CheckValidTag(collider.gameObject) && (parentGrabbed || constrGrabbed) && !inConnection && createdConnection)
            {
                targetSnapper = collider.gameObject;
                //StartPhantom();
                CreateConnectionObj();
            }
        }

        /// <summary>
        /// Is called when Trigger exits
        /// </summary>
        /// <param name="collider">Collider of the Trigger that exits</param>
        private void OnTriggerExit(Collider collider)
        {
            if (CheckValidTag(collider.gameObject) && (parentGrabbed || constrGrabbed) && !inConnection)
            {
                targetSnapper = null;
                //StopPhantom();
                DestroyConnectionObj();
            }
        }

        private void StartPhantom()
        {
            if (parentGrabbed)
            {
                transform.parent.GetComponent<PhantomObj>().StartDisplayPhantom(this.gameObject, targetSnapper, snapperID);
            }
            else if (constrGrabbed)
            {
                transform.parent.GetComponent<PhantomObj>().StartDisplayPhantom(this.gameObject, targetSnapper, snapperID);
            }
        }

        private void StopPhantom()
        {
            transform.parent.GetComponent<PhantomObj>().StopDisplayPhantom();
        }

        private void SetConstrListener(VRTK_InteractableObject objIO)
        {
            objIO.InteractableObjectGrabbed += InteractableConstructionGrabbed;
            objIO.InteractableObjectUngrabbed += InteractableConstructionUngrabbed;
        }

        private void RemoveConstrListener(VRTK_InteractableObject objIO)
        {
            objIO.InteractableObjectGrabbed -= InteractableConstructionGrabbed;
            objIO.InteractableObjectUngrabbed -= InteractableConstructionUngrabbed;
        }

        public void SetVRTKListener()
        {
            if (!transform.parent.tag.Equals("Start"))
            {
                parentObjIO = this.transform.parent.GetComponent<VRTK_InteractableObject>();
                parentObjIO.InteractableObjectGrabbed += InteractablePipeGrabbed;
                parentObjIO.InteractableObjectUngrabbed += InteractablePipeUngrabbed;
            }
        }

        public void RemoveVRTKListener()
        {
            if (parentObjIO != null)
            {
                parentObjIO.InteractableObjectGrabbed -= InteractablePipeGrabbed;
                parentObjIO.InteractableObjectUngrabbed -= InteractablePipeUngrabbed;
                parentObjIO = null;
            }
        }

        private void CreateConnectionObj()
        {
            GameObject connectionObj = Instantiate(Resources.Load("Prefab/Building Elements/Connection", typeof(GameObject))) as GameObject;
            connectionObj.GetComponent<Connection>().ConnectionSetup(this.gameObject, targetSnapper);
            createdConnection = false;
        }

        private void DestroyConnectionObj()
        {
            if (connection != null)
            {
                connection.GetComponent<Connection>().EndConnection();
                createdConnection = true;
            }
        }

        /// <summary>
        /// Checks if the Object has the valid Tag to continue
        /// </summary>
        /// <param name="tagCheck">Object which Tag should be checked</param>
        /// <returns>true if valid, else false</returns>
        private bool CheckValidTag(GameObject tagCheck)
        {
            return tagCheck.CompareTag(compareTag);
        }

        private void InteractablePipeGrabbed(object sender, InteractableObjectEventArgs e)
        {
            VRTK_InteractableObject grabbedInteractableObject = sender as VRTK_InteractableObject;
            parentGrabbed = true;
        }

        private void InteractablePipeUngrabbed(object sender, InteractableObjectEventArgs e)
        {
            VRTK_InteractableObject releasedInteractableObject = sender as VRTK_InteractableObject;
            if (targetSnapper != null && connection != null)
            {
                Debug.Log("Element Transform");
                Transformer.TransformToTarget(this.gameObject, targetSnapper, transform.parent.gameObject);
                targetSnapper = null;
                connection.GetComponent<Connection>().LockConnection();
            }
            parentGrabbed = false;
        }

        private void InteractableConstructionGrabbed(object sender, InteractableObjectEventArgs e)
        {
            VRTK_InteractableObject grabbedInteractableObject = sender as VRTK_InteractableObject;
            constrGrabbed = true;
        }

        private void InteractableConstructionUngrabbed(object sender, InteractableObjectEventArgs e)
        {
            VRTK_InteractableObject releasedInteractableObject = sender as VRTK_InteractableObject;
            if (updateConstr)
            {
                RemoveConstrListener(oldConstrObjIO);
                oldConstrObjIO = null;
                updateConstr = false;
            }
            if (targetSnapper != null && connection != null && !inConnection)
            {
                Debug.Log("Construction Transform");
                Transformer.TransformToTarget(this.gameObject, targetSnapper, construction);
                targetSnapper = null;
                connection.GetComponent<Connection>().LockConnection();
            }
            constrGrabbed = false;
        }
    }
}
