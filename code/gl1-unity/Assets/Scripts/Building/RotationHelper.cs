﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    public class RotationHelper : MonoBehaviour
    {
        public float globalX;
        public float globalY;
        public float globalZ;
        public float localX;
        public float localY;
        public float localZ;

        public bool startTransforming = false;
        public GameObject targetObj;

        void Update()
        {
            globalX = transform.rotation.eulerAngles.x;
            globalY = transform.rotation.eulerAngles.y;
            globalZ = transform.rotation.eulerAngles.z;
            localX = transform.localRotation.eulerAngles.x;
            localY = transform.localRotation.eulerAngles.y;
            localZ = transform.localRotation.eulerAngles.z;

            if (startTransforming)
            {
                Transformer.TransformToTarget(this.gameObject, targetObj, transform.parent.gameObject);
                startTransforming = false;
            }
        }
    }
}
