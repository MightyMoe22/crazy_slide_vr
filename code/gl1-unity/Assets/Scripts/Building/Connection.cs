﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace CrazySlide
{
    [RequireComponent(typeof(ConnectionAudio))]
    public class Connection : MonoBehaviour
    {
        static int nameNumber = 0;

        [Tooltip("Reference to the ConnectionIcon Child")]
        public GameObject connectionIcon;

        // The buildingElements which are connected through this Connection
        List<GameObject> buildingElements = new List<GameObject>();
        // Contains the Snapper which are connected by this Connection
        GameObject[] snappers = new GameObject[2];
        // The construction this connection is part of
        GameObject construction = null;
        // Index of the connection in the construction
        int connectionID = -1;

        GameObject constrToMerge = null;

        Transform positionToFollow = null;

        bool connectionLocked = false;
        bool flag = true;
        bool mergeRequest = false;

        private ConnectionAudio audioPlayer;

        void Start()
        {
            this.name = "Connection " + nameNumber;
            nameNumber++;
            connectionIcon.GetComponent<VRTK_InteractableObject>().InteractableObjectUsed += InteractableUse;
            audioPlayer = GetComponent<ConnectionAudio>();
        }

        void Update()
        {
            if (flag)
            {
                transform.position = positionToFollow.position;
                transform.rotation = positionToFollow.rotation;
            }
        }

        public List<GameObject> ReturnBuildingElements()
        {
            return buildingElements;
        }

        private void InteractableTouch(object sender, InteractableObjectEventArgs e)
        {
            VRTK_InteractableObject grabbedInteractableObject = sender as VRTK_InteractableObject;
            EndConnection();
        }

        private void InteractableUse(object sender, InteractableObjectEventArgs e)
        {
            VRTK_InteractableObject grabbedInteractableObject = sender as VRTK_InteractableObject;
            EndConnection();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aliSnapperObj">Alignment Snapper that initialized this connection</param> 
        /// <param name="tarSnapperObj">Target Snapper to connect to</param>
        /// <returns>GameObject which is the Construction Object that Connection is now part of</returns>
        public void ConnectionSetup(GameObject aliSnapperObj, GameObject tarSnapperObj)
        {
            positionToFollow = aliSnapperObj.transform;

            Snapper aliSnapper = aliSnapperObj.GetComponent<Snapper>();
            Snapper tarSnapper = tarSnapperObj.GetComponent<Snapper>();
            GameObject constrObj = CheckForExistingConstructions(aliSnapper, tarSnapper);

            aliSnapper.SetConnection(this.gameObject);
            tarSnapper.SetConnection(this.gameObject);

            SetValues(aliSnapperObj, tarSnapperObj);

            if (constrObj == null)
            {
                constrObj = Instantiate(Resources.Load("Prefab/Construction", typeof(GameObject))) as GameObject;
            }

            if (mergeRequest)
            {
                constrObj.GetComponent<Construction>().MergeConstruction(this.gameObject, constrToMerge);
                mergeRequest = false;
            }
            else
            {
                constrObj.GetComponent<Construction>().AddConnection(this.gameObject);
            }
        }

        public void SetConstructionInfo(GameObject constr, int index)
        {
            construction = constr;
            connectionID = index;
        }

        private GameObject CheckForExistingConstructions(Snapper aliSnapper, Snapper tarSnapper)
        {
            GameObject aliConstr = aliSnapper.construction;
            GameObject tarConstr = tarSnapper.construction;

            if (aliConstr != null && tarConstr != null)
            {
                mergeRequest = true;
                constrToMerge = tarConstr;
                return aliConstr;
            }
            else if (aliConstr != null)
            {
                return aliConstr;
            }
            else
            {
                return tarConstr;
            }
        }

        private void SetValues(GameObject aliSnapperObj, GameObject tarSnapperObj)
        {
            snappers[0] = tarSnapperObj;
            snappers[1] = aliSnapperObj;
            for (int i = 0; i < snappers.Length; i++)
            {
                buildingElements.Add(snappers[i].transform.parent.gameObject);
            }
        }

        public void LockConnection()
        {
            connectionLocked = true;
            flag = false;
            transform.position = positionToFollow.position;
            transform.rotation = positionToFollow.rotation;

            construction.GetComponent<Construction>().UpdatePendingIDs();

            for (int i = 0; i < 2; i++)
            {
                buildingElements[i].GetComponent<SnapperComs>().RemoveVRTK();
                snappers[i].GetComponent<Snapper>().SetInConnection(true);
            }

            if (constrToMerge != null)
            {
                Destroy(constrToMerge);
                constrToMerge = null;
            }
            audioPlayer.PlaySound(ConnectionAudio.Sound.Locking);
        }

        public void EndConnection()
        {
            if (connectionLocked)
            {
                UnlockConnection();
            }

            construction.GetComponent<Construction>().RemoveConnection(connectionID);

            for (int i = 0; i < snappers.Length; i++)
            {
                snappers[i].GetComponent<Snapper>().SetConnection(null);
            }
            connectionIcon.GetComponent<Renderer>().enabled = false;
            Destroy(this.gameObject, 0.3f);
        }

        private void UnlockConnection()
        {
            connectionLocked = false;
            for (int i = 0; i < snappers.Length; i++)
            {
                snappers[i].GetComponent<Snapper>().SetInConnection(false);
            }
            audioPlayer.PlaySound(ConnectionAudio.Sound.Unlocking);
        }
    }
}
