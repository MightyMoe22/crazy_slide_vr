﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    public class Transformer : MonoBehaviour
    {
        /// <summary>
        /// Called to start the transforming process to line up alignmentObj and targetObj
        /// </summary>
        /// <param name="alignmentObj">object whose position relativ to the targetObj will define the new position of movingObj</param>
        /// <param name="targetObj">object to move to</param>
        /// <param name="moveingObj">object that gets moved</param>
        public static void TransformToTarget(GameObject alignmentObj, GameObject targetObj, GameObject moveingObj)
        {
            Transform moverTrans;
            GameObject transformHelper = CreatTransformHelper(alignmentObj, moveingObj);
            moverTrans = transformHelper.transform;
            PerformTransformation(alignmentObj, targetObj, moverTrans);
            DestroyTransformHelper(transformHelper, moveingObj);
        }

        protected static GameObject CreatTransformHelper(GameObject alignmentObj, GameObject moveingObj)
        {
            GameObject transformHelper = new GameObject("TransformHelper for: " + moveingObj.name);
            transformHelper.transform.position = alignmentObj.transform.position;
            transformHelper.transform.rotation = alignmentObj.transform.rotation;
            moveingObj.transform.SetParent(transformHelper.transform);

            return transformHelper;
        }

        protected static void DestroyTransformHelper(GameObject transformHelper, GameObject moverObj)
        {
            moverObj.transform.SetParent(null);
            Destroy(transformHelper);
        }

        protected static void PerformTransformation(GameObject alignmentObj, GameObject targetObj, Transform moverTrans)
        {
            bool storedKinematicState = alignmentObj.transform.parent.GetComponent<Rigidbody>().isKinematic;
            alignmentObj.transform.parent.GetComponent<Rigidbody>().isKinematic = true;

            if (alignmentObj != null)
            {
                MoveToTarget(alignmentObj, targetObj, moverTrans);
            }

            alignmentObj.transform.parent.GetComponent<Rigidbody>().isKinematic = storedKinematicState;
        }

        /// <summary>
        /// Uses target and alignment objects as reference so the move object can be transformed to line up target and alignment
        /// </summary>
        /// <param name="targetObj">Object where the alignmentObj should aligne to</param>
        /// <param name="alignmentObj">Object that should be aligned to targetObj</param>
        /// <param name="moverTrans">Mover transform which should be modified so that alignment and target object overlap</param>
        protected static void MoveToTarget(GameObject alignmentObj, GameObject targetObj, Transform moverTrans)
        {
            float savedZRotation = alignmentObj.transform.eulerAngles.z;

            //Rotate moverTrans so, the alignmentObj z axis points in the opposite direction from targetObj z axis
            moverTrans.rotation = Quaternion.FromToRotation(alignmentObj.transform.forward, -targetObj.transform.forward) * moverTrans.rotation;

            //Add change to moverTrans position, relative to alignmentObj's distance to targetObj's position
            moverTrans.position += targetObj.transform.position - alignmentObj.transform.position;

            //Line up all axis to each other
            moverTrans.rotation = Quaternion.FromToRotation(alignmentObj.transform.up, targetObj.transform.up) * moverTrans.rotation;
            moverTrans.rotation = Quaternion.FromToRotation(alignmentObj.transform.right, targetObj.transform.right) * moverTrans.rotation;
            moverTrans.rotation = Quaternion.FromToRotation(alignmentObj.transform.forward, -targetObj.transform.forward) * moverTrans.rotation;

            //Create final Rotation
            Vector3 relativAngle = new Vector3(moverTrans.transform.eulerAngles.x, moverTrans.transform.eulerAngles.y, savedZRotation);
            moverTrans.transform.rotation = Quaternion.RotateTowards(moverTrans.transform.rotation, Quaternion.Euler(relativAngle), 360);
        }
    }
}
