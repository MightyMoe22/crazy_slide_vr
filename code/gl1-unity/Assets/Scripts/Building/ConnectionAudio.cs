﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    [RequireComponent(typeof(AudioSource))]
    public class ConnectionAudio : MonoBehaviour
    {
        public enum Sound { Locking, Unlocking }

        public AudioClip lockSound;
        public AudioClip unlockSound;

        private AudioSource source;
        void Awake()
        {
            source = GetComponent<AudioSource>();
        }

        public void PlaySound(Sound sound)
        {
            AudioClip playClip;
            if (sound == Sound.Locking)
                playClip = lockSound;
            else
                playClip = unlockSound;

            source.PlayOneShot(playClip, 1F);
        }
    }
}
