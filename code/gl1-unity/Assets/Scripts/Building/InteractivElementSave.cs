﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.GrabAttachMechanics;
using VRTK.SecondaryControllerGrabActions;

namespace CrazySlide
{
    public class InteractableElementSave
    {
        bool disableWhenIdle;
        VRTK_InteractableObject.AllowedController allowedNearTouchControllers;
        VRTK_InteractableObject.AllowedController allowedTouchControllers;
        Collider[] ignoredColliders;

        bool isGrabbable;
        bool holdButtonToGrab;
        bool stayGrabbedOnTeleport;
        VRTK_InteractableObject.ValidDropTypes validDrop;
        VRTK_ControllerEvents.ButtonAlias grabOverrideButton;
        VRTK_InteractableObject.AllowedController allowedGrabControllers;
        VRTK_BaseGrabAttach grabAttachMechanicScript;
        VRTK_BaseGrabAction secondaryGrabActionScript;
        bool isUsable;
        bool holdButtonToUse;
        bool useOnlyIfGrabbed;
        bool pointerActivatesUseAction;
        VRTK_ControllerEvents.ButtonAlias useOverrideButton;
        VRTK_InteractableObject.AllowedController allowedUseControllers;
        VRTK.Highlighters.VRTK_BaseHighlighter objectHighlighter;
        Color touchHighlightColor;

        public InteractableElementSave(GameObject interPipe)
        {
            if (interPipe.GetComponent<VRTK_InteractableObject>() != null)
            {
                Save(interPipe);
            }
        }

        public void Save(GameObject interPipe)
        {
            VRTK_InteractableObject interPipeScript = interPipe.GetComponent<VRTK_InteractableObject>();
            disableWhenIdle = false;
            allowedNearTouchControllers = interPipeScript.allowedNearTouchControllers;
            allowedTouchControllers = interPipeScript.allowedTouchControllers;
            ignoredColliders = interPipeScript.ignoredColliders;
            isGrabbable = interPipeScript.isGrabbable;
            holdButtonToGrab = interPipeScript.holdButtonToGrab;
            stayGrabbedOnTeleport = interPipeScript.stayGrabbedOnTeleport;
            validDrop = interPipeScript.validDrop;
            grabOverrideButton = interPipeScript.grabOverrideButton;
            allowedGrabControllers = interPipeScript.allowedGrabControllers;
            grabAttachMechanicScript = interPipeScript.grabAttachMechanicScript;
            secondaryGrabActionScript = interPipeScript.secondaryGrabActionScript;
            isUsable = interPipeScript.isUsable;
            holdButtonToUse = interPipeScript.holdButtonToUse;
            useOnlyIfGrabbed = interPipeScript.useOnlyIfGrabbed;
            pointerActivatesUseAction = interPipeScript.pointerActivatesUseAction;
            useOverrideButton = interPipeScript.useOverrideButton;
            allowedUseControllers = interPipeScript.allowedUseControllers;
            objectHighlighter = interPipeScript.objectHighlighter;
            touchHighlightColor = interPipeScript.touchHighlightColor;
        }

        public void Restore(GameObject interPipe)
        {
            VRTK_InteractableObject interPipeScript = interPipe.GetComponent<VRTK_InteractableObject>();

            interPipeScript.disableWhenIdle = disableWhenIdle;
            interPipeScript.allowedNearTouchControllers = allowedNearTouchControllers;
            interPipeScript.allowedTouchControllers = allowedTouchControllers;
            interPipeScript.ignoredColliders = ignoredColliders;
            interPipeScript.isGrabbable = isGrabbable;
            interPipeScript.holdButtonToGrab = holdButtonToGrab;
            interPipeScript.stayGrabbedOnTeleport = stayGrabbedOnTeleport;
            interPipeScript.validDrop = validDrop;
            interPipeScript.grabOverrideButton = grabOverrideButton;
            interPipeScript.allowedGrabControllers = allowedGrabControllers;
            interPipeScript.grabAttachMechanicScript = grabAttachMechanicScript;
            interPipeScript.secondaryGrabActionScript = secondaryGrabActionScript;
            interPipeScript.isUsable = isUsable;
            interPipeScript.holdButtonToUse = holdButtonToUse;
            interPipeScript.useOnlyIfGrabbed = useOnlyIfGrabbed;
            interPipeScript.pointerActivatesUseAction = pointerActivatesUseAction;
            interPipeScript.useOverrideButton = useOverrideButton;
            interPipeScript.allowedUseControllers = allowedUseControllers;
            interPipeScript.objectHighlighter = objectHighlighter;
            interPipeScript.touchHighlightColor = touchHighlightColor;
        }
    }
}
