﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace CrazySlide
{
    public class Construction : MonoBehaviour
    {
        enum RemoveOptions
        {
            HeadRemove, TailRemove, MiddleRemove
        }

        static int nameNumber = 0;

        public GameObject connections = null;
        public GameObject elements = null;

        // Contains all connections in this construction
        [HideInInspector]
        public List<GameObject> connectionList = new List<GameObject>();
        // Contains all elements in this construction
        [HideInInspector]
        public List<GameObject> elementList = new List<GameObject>();

        List<GameObject> tempBuildElem;

        List<GameObject> pendingCons = new List<GameObject>();
        List<GameObject> pendingElem = new List<GameObject>();

        public bool isOrigin = false;
        [Tooltip("Needed if isOrigin is true")]
        public GameObject originElement;

        bool makeReadyForDestroy = false;
        bool updateIndex = false;

        // Use this for initialization
        void Start()
        {
            this.name = "Construction " + nameNumber;
            nameNumber++;
        }

        public void OriginSetUp(bool status)
        {
            this.gameObject.GetComponent<VRTK_InteractableObject>().isGrabbable = !status;
            isOrigin = status;
        }

        public void AddConnection(GameObject connectionObj)
        {
            Connection connection = connectionObj.GetComponent<Connection>();
            tempBuildElem = new List<GameObject>(connection.ReturnBuildingElements());
            int indexWhereToAdd = CheckElements(tempBuildElem);

            if (indexWhereToAdd == 0)
            {
                updateIndex = true;
                connectionList.Insert(0, connectionObj);
            }
            else
            {
                connectionList.Add(connectionObj);
            }

            connection.SetConstructionInfo(this.gameObject, connectionList.IndexOf(connectionObj));
            pendingCons.Add(connectionObj);

            AddElements(tempBuildElem, indexWhereToAdd);

            if (updateIndex)
            {
                UpdateIndex();
            }
        }


        public void AddElements(List<GameObject> elementObjs, int indexWhereToAdd)
        {
            foreach (GameObject gameOb in elementObjs)
            {
                CheckIfElementIsStart(gameOb, true);
            }
            foreach (GameObject gameOb in elementObjs)
            {
                if (indexWhereToAdd == 0)
                {
                    elementList.Insert(0, gameOb);
                }
                else
                {
                    elementList.Add(gameOb);
                }
                gameOb.GetComponent<SnapperComs>().SetConstructionInfo(this.gameObject, elementList.IndexOf(gameOb));
                pendingElem.Add(gameOb);
            }
            tempBuildElem = null;
        }

        private int CheckElements(List<GameObject> buildingElements)
        {
            int indexWhereToAdd = -1;
            GameObject gameObjTemp = null;
            foreach (GameObject gameObj in buildingElements)
            {
                if (elementList.Contains(gameObj))
                {
                    indexWhereToAdd = elementList.IndexOf(gameObj);
                    gameObjTemp = gameObj;
                }
            }
            if (gameObjTemp != null)
            {
                buildingElements.Remove(gameObjTemp);
            }
            return indexWhereToAdd;
        }

        public void MergeConstruction(GameObject connectionObj, GameObject constrToMerge)
        {
            AddConnection(connectionObj);

            Construction constrToMergeComp = constrToMerge.GetComponent<Construction>();
            List<GameObject> tempConnectionList = constrToMergeComp.connectionList;

            Connection connection = connectionObj.GetComponent<Connection>();
            tempBuildElem = new List<GameObject>(connection.ReturnBuildingElements());
            int indexWhereToAdd = constrToMergeComp.CheckElements(tempBuildElem);

            if (indexWhereToAdd == 0)
            {
                for (int i = 0; i < tempConnectionList.Count; i++)
                {
                    AddConnection(tempConnectionList[i]);
                }
            }
            else
            {
                for (int i = tempConnectionList.Count - 1; i >= 0; i--)
                {
                    AddConnection(tempConnectionList[i]);
                }
            }
        }

        private void SetObjectHierarchy(GameObject gameObj, GameObject parentObj)
        {
            gameObj.transform.SetParent(parentObj.transform);
        }

        private void UpdateIndex()
        {
            foreach (GameObject gameObj in connectionList)
            {
                gameObj.GetComponent<Connection>().SetConstructionInfo(this.gameObject, connectionList.IndexOf(gameObj));
            }

            foreach (GameObject gameObj in elementList)
            {
                gameObj.GetComponent<SnapperComs>().SetConstructionInfo(this.gameObject, elementList.IndexOf(gameObj));
            }
            updateIndex = false;
        }

        public void UpdatePendingIDs()
        {
            foreach (GameObject gameObj in pendingCons)
            {
                if (gameObj != null)
                {
                    SetObjectHierarchy(gameObj, connections);
                }
            }
            pendingCons.Clear();
            foreach (GameObject gameObj in pendingElem)
            {
                if (gameObj != null)
                {
                    SetObjectHierarchy(gameObj, elements);
                }
            }
            pendingElem.Clear();
        }


        /// <summary>
        /// Removes connection from construction
        /// </summary>
        /// <param name="connection"></param>
        public void RemoveConnection(int connectionID)
        {
            RemoveOptions option = CheckForCase(connectionID);
            CheckIfEmptyAfterRemoval();
            switch (option)
            {
                case RemoveOptions.HeadRemove:
                    RemoveHead(connectionID);
                    break;
                case RemoveOptions.MiddleRemove:
                    SplitConstructionAt(connectionID);
                    break;
                case RemoveOptions.TailRemove:
                    RemoveTail(connectionID);
                    break;
            }
            if (updateIndex)
            {
                UpdateIndex();
            }
            if (makeReadyForDestroy)
            {
                Destroy(this.gameObject);
            }
        }

        private void RemoveTail(int connectionID)
        {
            connectionList[connectionID].transform.SetParent(null);
            connectionList[connectionID].GetComponent<Connection>().SetConstructionInfo(null, -1);
            connectionList.RemoveAt(connectionID);

            CheckIfElementIsStart(elementList[connectionID + 1], false);
            elementList[connectionID + 1].transform.SetParent(null);
            elementList[connectionID + 1].GetComponent<SnapperComs>().SetConstructionInfo(null, -1);
            elementList.RemoveAt(connectionID + 1);

            if (makeReadyForDestroy)
            {
                elementList[connectionID].transform.SetParent(null);
                elementList[connectionID].GetComponent<SnapperComs>().SetConstructionInfo(null, -1);
                elementList.RemoveAt(connectionID);
            }
        }

        private void RemoveHead(int connectionID)
        {
            connectionList[connectionID].transform.SetParent(null);
            connectionList[connectionID].GetComponent<Connection>().SetConstructionInfo(null, -1);
            connectionList.RemoveAt(connectionID);

            CheckIfElementIsStart(elementList[connectionID], false);
            elementList[connectionID].transform.SetParent(null);
            elementList[connectionID].GetComponent<SnapperComs>().SetConstructionInfo(null, -1);
            elementList.RemoveAt(connectionID);

            if (makeReadyForDestroy)
            {
                elementList[connectionID + 1].transform.SetParent(null);
                elementList[connectionID + 1].GetComponent<SnapperComs>().SetConstructionInfo(null, -1);
                elementList.RemoveAt(connectionID + 1);
            }
        }

        private RemoveOptions CheckForCase(int connectionID)
        {
            RemoveOptions temp;
            if (connectionList.Count - 1 == connectionID)
            {
                temp = RemoveOptions.TailRemove;
            }
            else if (0 == connectionID)
            {
                updateIndex = true;
                temp = RemoveOptions.HeadRemove;
            }
            else
            {
                temp = RemoveOptions.MiddleRemove;
            }
            return temp;
        }

        /// <summary>
        /// If there is no connection left in the construction, the construction gets deleted (except if consturction is origin).
        /// </summary>
        /// <returns>true if empty else false</returns>
        private void CheckIfEmptyAfterRemoval()
        {
            if (connectionList.Count <= 1)
            {
                makeReadyForDestroy = true;
            }
        }

        /// <summary>
        /// Splits a construction into two sperated constructions
        /// </summary>
        /// <param name="connectionID">Index where to splitt</param>
        private void SplitConstructionAt(int connectionID)
        {
            GameObject newConstrObj = Instantiate(Resources.Load("Prefab/Construction", typeof(GameObject))) as GameObject;
            Construction newConstrComp = newConstrObj.GetComponent<Construction>();

            for (int i = connectionID + 1; i < connectionList.Count; i++)
            {
                newConstrComp.AddConnection(connectionList[i]);
            }
            int conRange = connectionList.Count - connectionID;
            int elmRange = elementList.Count - (connectionID + 1);
            connectionList.RemoveRange(connectionID, conRange);
            elementList.RemoveRange(connectionID + 1, elmRange);
            newConstrComp.UpdatePendingIDs();
            CheckIfOriginIsStillPartOfConstrucktion();
        }

        private void CheckIfElementIsStart(GameObject element, bool status)
        {
            if (element.tag.Equals("Start"))
            {
                OriginSetUp(status);
            }
        }

        private void CheckIfOriginIsStillPartOfConstrucktion()
        {
            bool isStillPart = false;
            foreach (GameObject gameOb in elementList)
            {
                if (gameOb.tag.Equals("Start"))
                {
                    isStillPart = true;
                    break;
                }
            }
            OriginSetUp(isStillPart);
        }
    }
}
