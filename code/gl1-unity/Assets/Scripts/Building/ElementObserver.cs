﻿using UnityEngine;
using VRTK;

namespace CrazySlide
{
    [RequireComponent(typeof(VRTK_InteractGrab))]
    public class ElementObserver : MonoBehaviour
    {
        public static bool anElementIsGrabbed = false;
        private static GameObject grabbedObject = null;

        private VRTK_InteractGrab vrtkIG;
        private void Start()
        {
            vrtkIG = GetComponent<VRTK_InteractGrab>();
            vrtkIG.ControllerStartGrabInteractableObject += ControllerStartGrabInteractableObject;
            vrtkIG.ControllerUngrabInteractableObject += ControllerUngrabInteractableObject;
        }

        private void Update()
        {
            if (anElementIsGrabbed && (vrtkIG.GetGrabbedObject() == null || !vrtkIG.GetGrabbedObject().Equals(grabbedObject)))
            {
                vrtkIG.enabled = false;
            }
            else
            {
                vrtkIG.enabled = true;
            }
        }

        private void ControllerStartGrabInteractableObject(object sender, ObjectInteractEventArgs e)
        {
            anElementIsGrabbed = true;
            grabbedObject = e.target;
        }

        private void ControllerUngrabInteractableObject(object sender, ObjectInteractEventArgs e)
        {
            anElementIsGrabbed = false;
            grabbedObject = null;
        }

        public void OnDestroy()
        {
            vrtkIG.ControllerStartGrabInteractableObject -= ControllerStartGrabInteractableObject;
            vrtkIG.ControllerUngrabInteractableObject -= ControllerUngrabInteractableObject;
        }

    }
}
