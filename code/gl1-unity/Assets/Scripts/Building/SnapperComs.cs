﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace CrazySlide
{
    /// <summary>
    /// SnapperComs controlls the sharing of Information between Snapper Objects in a building Element
    /// </summary>
    public class SnapperComs : MonoBehaviour
    {

        [Tooltip("Reference to the other snapper")]
        public List<GameObject> snapper = new List<GameObject>();

        [HideInInspector]
        public bool elementGrabbed = false;
        [HideInInspector]
        public bool constrGrabbed = false;
        [HideInInspector]
        public GameObject construction = null;
        // Index of the building element in the construction
        int elementID = -1;
        // Container for saving the values of the VRTK_InteraktableObject script
        InteractableElementSave interactableElementSaves;

        void Start()
        {
            FindObjectwithTag("SnapConnector");
            DeployComs();
            interactableElementSaves = new InteractableElementSave(this.gameObject);
        }

        void Update()
        {
            if (GetComponent<VRTK_InteractableObject>() != null)
            {
                elementGrabbed = GetComponent<VRTK_InteractableObject>().IsGrabbed();
            }
            if (construction != null)
            {
                constrGrabbed = construction.GetComponent<VRTK_InteractableObject>().IsGrabbed();
            }
        }

        public void SetConstructionInfo(GameObject constr, int index)
        {
            construction = constr;
            elementID = index;

            for (int i = 0; i < snapper.Count; i++)
            {
                snapper[i].GetComponent<Snapper>().SetConstruction(construction);
            }
            if (GetComponent<VRTK_InteractableObject>() == null && constr == null && !tag.Equals("Start"))
            {
                Debug.Log("Obj Name: " + this.name + " -RestoreVRTK");
                RestoreVRTK();
            }
        }

        public void RemoveVRTK()
        {
            if (this.gameObject.GetComponent<VRTK_InteractableObject>() != null)
            {
                for (int i = 0; i < snapper.Count; i++)
                {
                    snapper[i].GetComponent<Snapper>().RemoveVRTKListener();
                }
                elementGrabbed = false;
                Destroy(this.gameObject.GetComponent<VRTK_InteractableObject>());
            }
        }

        private void RestoreVRTK()
        {
            this.gameObject.AddComponent<VRTK_InteractableObject>();
            this.gameObject.GetComponent<VRTK_InteractableObject>().enabled = true;
            interactableElementSaves.Restore(this.gameObject);
            for (int i = 0; i < snapper.Count; i++)
            {
                snapper[i].GetComponent<Snapper>().SetVRTKListener();
            }
        }

        private void DeployComs()
        {
            for (int i = 0; i < snapper.Count; i++)
            {
                snapper[i].GetComponent<Snapper>().SetSnapperComs(this.gameObject);
            }
        }

        private void FindObjectwithTag(string tag)
        {
            snapper.Clear();
            Transform parent = transform;
            GetChildObject(parent, tag);
        }

        private void GetChildObject(Transform parent, string tag)
        {
            for (int i = 0; i < parent.childCount; i++)
            {
                Transform child = parent.GetChild(i);
                if (child.tag == tag)
                {
                    snapper.Add(child.gameObject);
                }
            }
        }
    }
}
