﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace CrazySlide
{
    [RequireComponent(typeof(VRTK_ControllerHighlighter))]
    public class TutorialHighlightController : MonoBehaviour
    {
        public Color highlightColor;

        private VRTK_ControllerHighlighter controllerHighlighter;

        private void Awake()
        {
            controllerHighlighter = GetComponent<VRTK_ControllerHighlighter>();
        }

        public void HighlightTrigger()
        {
            controllerHighlighter.highlightTrigger = highlightColor;
        }

        public void HighlightGrip()
        {
            controllerHighlighter.highlightGrip = highlightColor;
        }

        public void HighlightTouchpad()
        {
            controllerHighlighter.highlightTouchpad = highlightColor;

        }

    }
}
