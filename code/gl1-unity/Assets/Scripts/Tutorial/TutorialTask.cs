﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    public struct TutorialTaskEventArgs
    {
        public GameObject tutorialTask;
    }

    public delegate void TutorialTaskEventHandler(object sender, TutorialTaskEventArgs e);

    public abstract class TutorialTask : MonoBehaviour
    {
        public event TutorialTaskEventHandler TaskComplete;
        protected bool EventIsNull()
        {
            return TaskComplete == null;
        }
        public virtual void OnTaskComplete(TutorialTaskEventArgs e)
        {
            if (!EventIsNull())
            {
                TaskComplete(this, e);
            }
        }
    }
}
