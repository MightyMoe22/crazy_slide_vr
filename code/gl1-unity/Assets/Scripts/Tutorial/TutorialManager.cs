﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    public class TutorialManager : MonoBehaviour
    {
        public TutorialHighlightController leftController;
        public TutorialHighlightController rightController;

        public List<TutorialTask> Tasks = new List<TutorialTask>();

        public bool tutorialEnabled = false;


        private void Awake()
        {
            if (tutorialEnabled)
            {
                foreach(TutorialTask task in Tasks)
                {
                    task.TaskComplete += TaskCompleteListener;
                }
            }
        }

        private void TaskCompleteListener(object obj, TutorialTaskEventArgs e)
        {

        }
        private void Update()
        {
            if (tutorialEnabled)
            {
                leftController.HighlightGrip();
                leftController.HighlightTouchpad();
                leftController.HighlightTrigger();
                rightController.HighlightGrip();
                rightController.HighlightTouchpad();
                rightController.HighlightTrigger();
            }
        }
    }
}
