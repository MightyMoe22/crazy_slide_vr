﻿using UnityEngine;
using VRTK.Controllables;
using VRTK.Controllables.ArtificialBased;

namespace CrazySlide
{
    [RequireComponent(typeof(SwitchGameMode))]
    [RequireComponent(typeof(SwitchPlayerPosition))]
    public class StartLever : MonoBehaviour
    {
        public VRTK_ArtificialRotator artificialRotator;
        public Countdown countdown;

        private SwitchGameMode switchGameMode;
        private SwitchPlayerPosition switchPlayerPosition;
        private float minAngle;

        private string outputOnMax = "Maximum Reached";
        private string outputOnMin = "Minimum Reached";

        void Start()
        {
            switchGameMode = GetComponent<SwitchGameMode>();
            switchPlayerPosition = GetComponent<SwitchPlayerPosition>();
            minAngle = artificialRotator.angleLimits.minimum;
        }

        public void ResetLever()
        {
            artificialRotator.SetValue(minAngle);
            switchGameMode.GetWorldBuildReady();
            switchPlayerPosition.SwitchPlayerToVRHeadset();
            countdown.StopCountdown();
        }

        protected virtual void OnEnable()
        {
            artificialRotator = (artificialRotator == null ? GetComponent<VRTK_ArtificialRotator>() : artificialRotator);
            artificialRotator.MaxLimitReached += MaxLimitReached;
            artificialRotator.MinLimitReached += MinLimitReached;
        }

        protected virtual void MaxLimitReached(object sender, ControllableEventArgs e)
        {
            if (outputOnMax != "")
            {
                Debug.Log(outputOnMax);
            }
            switchGameMode.GetWorldSlideReady();
            switchPlayerPosition.SwitchPlayerToObject();
            countdown.StartCountdown();
        }

        protected virtual void MinLimitReached(object sender, ControllableEventArgs e)
        {
            if (outputOnMin != "")
            {
                //Debug.Log(outputOnMin);
            }
        }
    }
}
