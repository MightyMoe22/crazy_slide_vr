﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    public class SwitchPlayerPosition : MonoBehaviour
    {
        [Tooltip("Location where the playerSlideObject should be positioned")]
        public Transform playerSpawn;
        [Tooltip("The playerSlideObject")]
        public GameObject playerBody;

        public Transform cameraPositionAtStart;
        [Tooltip("CameraRig Object from the SteamVR from VRTK-Player Object")]
        public GameObject cameraRig;
        [Tooltip("SteamVR Object from the VRTK-Player Object")]
        public GameObject SteamVRObj;
        public Transform SteamVRAtStart;
        [Tooltip("CameraEye Object from the cameraRig")]
        public GameObject cameraEye;
        public PositionOffsetCounter positionOffsetCounter;

        private Vector3 rigOriginPosition;
        private Vector3 cameraOriginPosition;
        private Vector3 steamVROriginPosition;
        private Quaternion rigOriginRotation;
        private Quaternion cameraOriginRotation;
        private Quaternion steamVROriginRotation;
        private Transform rigPrevParent;
        private Transform cameraPrevParent;
        private Transform steamVRPrevParent;

        void Start()
        {
            playerBody.SetActive(false);
        }

        public void SwitchPlayerToObject()
        {

            SavePostion();
            TransformToStart();
            playerBody.SetActive(true);
            playerBody.GetComponent<Rigidbody>().isKinematic = false;
            playerBody.GetComponent<Rigidbody>().useGravity = true;
            GameMode.inBuildMode = false;
        }

        private void SavePostion()
        {
            rigOriginPosition = new Vector3(cameraRig.transform.localPosition.x, cameraRig.transform.localPosition.y, cameraRig.transform.localPosition.z);
            rigOriginRotation = new Quaternion(cameraRig.transform.localRotation.x, cameraRig.transform.localRotation.y, cameraRig.transform.localRotation.z, cameraRig.transform.localRotation.w);
            cameraOriginPosition = new Vector3(cameraEye.transform.localPosition.x, cameraEye.transform.localPosition.y, cameraEye.transform.localPosition.z);
            cameraOriginRotation = new Quaternion(cameraEye.transform.localRotation.x, cameraEye.transform.localRotation.y, cameraEye.transform.localRotation.z, cameraEye.transform.localRotation.w);
            steamVROriginPosition = new Vector3(SteamVRObj.transform.localPosition.x, SteamVRObj.transform.localPosition.y, SteamVRObj.transform.localPosition.z);
            steamVROriginRotation = new Quaternion(SteamVRObj.transform.localRotation.x, SteamVRObj.transform.localRotation.y, SteamVRObj.transform.localRotation.z, cameraRig.transform.localRotation.w);
            rigPrevParent = cameraRig.transform.parent;
            cameraPrevParent = cameraEye.transform.parent;
            steamVRPrevParent = SteamVRObj.transform.parent;
        }

        public void SwitchPlayerToVRHeadset()
        {
            TransformToLever();
            ResetPlayer();
            GameMode.inBuildMode = true;
        }

        private void TransformToStart()
        {
            positionOffsetCounter.enabled = true;
            SteamVRObj.transform.SetParent(playerBody.transform);
            SteamVRObj.transform.localPosition = new Vector3(0, 0, 0);
            SteamVRObj.transform.localScale = new Vector3(1, 1, 1);
            //cameraRig.transform.SetParent(playerBody.transform);
            Debug.Log("Before" + cameraRig.transform.localPosition);
            cameraRig.transform.localPosition = new Vector3(0, 0, 0);
            Debug.Log("After" + cameraRig.transform.localPosition);
            //cameraEye.transform.SetParent(cameraPositionAtStart);
            cameraEye.transform.localPosition = new Vector3(0, 0, 0);
        }

        private void TransformToLever()
        {
            positionOffsetCounter.enabled = false;
            SteamVRObj.transform.SetParent(steamVRPrevParent.transform);
            SteamVRObj.transform.localPosition = steamVROriginPosition;
            SteamVRObj.transform.localRotation = steamVROriginRotation;
            SteamVRObj.transform.localScale = new Vector3(1, 1, 1);
            //cameraRig.transform.SetParent(rigPrevParent);
            cameraRig.transform.localPosition = rigOriginPosition;
            cameraRig.transform.localRotation = rigOriginRotation;
            cameraEye.transform.SetParent(cameraPrevParent);
            cameraEye.transform.localPosition = cameraOriginPosition;
            cameraEye.transform.localRotation = cameraOriginRotation;
        }

        private void ResetPlayer()
        {
            playerBody.SetActive(false);
            playerBody.GetComponent<Rigidbody>().isKinematic = true;
            playerBody.GetComponent<Rigidbody>().useGravity = false;
            playerBody.transform.position = playerSpawn.transform.position;
            playerBody.transform.rotation = playerSpawn.transform.rotation;
        }
    }
}
