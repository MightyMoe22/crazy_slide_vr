﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    public class Finish : MonoBehaviour
    {
        public LevelName levelnameToUnlock;
        public StartLever startLever;
        public float countdownTime = 1.0f;

        private bool startCountdown;
        private float activeCountdownTime;

        private void Update()
        {
            if (startCountdown)
            {
                activeCountdownTime -= Time.deltaTime;

                if (activeCountdownTime <= 0.0f)
                {
                    startCountdown = false;
                    startLever.ResetLever();
                }
            }
        }

        void OnTriggerEnter(Collider col)
        {
            if (col.tag.Equals("Player"))
            {
                activeCountdownTime = countdownTime;
                startCountdown = true;
                LevelLockState.UnlockLevel(levelnameToUnlock);
            }
        }
    }
}
