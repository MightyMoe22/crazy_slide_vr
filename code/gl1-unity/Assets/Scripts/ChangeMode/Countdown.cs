﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CrazySlide
{
    public class Countdown : MonoBehaviour
    {
        public AudioClip beep;
        public AudioClip lastBeep;
        public VelocityBoost booster;
        public Text countdownDisplay;
        public AudioSource audioSource;
        public float countdownTime = 3.0f;

        private float activeCountdownTime = -1.0f;
        private int lastSecond = -1;
        private bool readyToGo = false;

        void Update()
        {
            if (activeCountdownTime >= 0.0f && readyToGo)
            {
                activeCountdownTime -= Time.deltaTime;
                int tempTime = (int)activeCountdownTime;
                if (tempTime != 0 && tempTime != lastSecond)
                {
                    audioSource.PlayOneShot(beep, 1.0f);
                    lastSecond = tempTime;
                }
                else if (tempTime == 0 && lastSecond != -1)
                {
                    audioSource.PlayOneShot(lastBeep, 1.0f);
                    lastSecond = -1;
                }

                countdownDisplay.text = tempTime.ToString();

                if (activeCountdownTime <= 0.0f)
                {
                    booster.BoostActive = true;
                    countdownDisplay.enabled = false;
                }
            }
        }

        public void StartCountdown()
        {
            countdownDisplay.enabled = true;
            activeCountdownTime = countdownTime;
            readyToGo = true;
        }

        public void StopCountdown()
        {
            booster.BoostActive = false;
            readyToGo = false;
            countdownDisplay.enabled = false;
            activeCountdownTime = 0f;
            lastSecond = -1;
        }
    }
}
