﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    public class PositionOffsetCounter : MonoBehaviour
    {
        public Transform movingObject;
        public Transform counterObject;

        private Vector3 origin;

        private void Start()
        {
            origin = new Vector3(counterObject.localPosition.x, counterObject.localPosition.y, counterObject.localPosition.z);
        }

        private void Update()
        {
            CounterMovement();
        }

        private void CounterMovement()
        {
            Vector3 tempPos = new Vector3(origin.x + (-movingObject.localPosition.x),
                                        origin.y + (-movingObject.localPosition.y),
                                        origin.z + (-movingObject.localPosition.z));
            counterObject.transform.localPosition = tempPos;
        }
    }
}
