﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    public class VelocityBoost : MonoBehaviour
    {
        public float forceAmount = 500;
        private bool _boostActive = false;
        public bool BoostActive { get { return _boostActive; } set { _boostActive = value; } }

        void OnTriggerStay(Collider col)
        {
            if (BoostActive && col.tag.Equals("Player"))
            {
                col.gameObject.GetComponent<Rigidbody>().AddForce(-transform.right * forceAmount, ForceMode.Force);
            }
        }
    }
}
