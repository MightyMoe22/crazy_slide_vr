﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
namespace CrazySlide
{
    public class ExitSlideMode : MonoBehaviour
    {
        public VRTK_ControllerEvents leftController;
        public VRTK_ControllerEvents rightController;
        public StartLever startLever;

        void Update()
        {
            if (CheckBothTriggerArePressed() && !GameMode.inBuildMode)
            {
                startLever.ResetLever();
            }
        }

        private bool CheckBothTriggerArePressed()
        {
            bool bothPressed = false;
            if (leftController.triggerPressed && rightController.triggerPressed)
            {
                bothPressed = true;
            }
            return bothPressed;
        }
    }
}
