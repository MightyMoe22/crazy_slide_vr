﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    public class PlayerHitFloor : MonoBehaviour
    {
        public StartLever startLever;

        void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag.Equals("Floor"))
            {
                startLever.ResetLever();
            }
        }
    }
}
