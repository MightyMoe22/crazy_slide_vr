﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    [RequireComponent(typeof(Rigidbody))]
    public class CenterOfMass : MonoBehaviour
    {
        public Vector3 newCenterOfMass;
        public bool awake;
        private Rigidbody rBody;

        void Start()
        {
            rBody = GetComponent<Rigidbody>();
        }

        void Update()
        {
            rBody.centerOfMass = newCenterOfMass;
            rBody.WakeUp();
            awake = !rBody.IsSleeping();
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(transform.position + transform.rotation * newCenterOfMass, 1f);
        }
    }
}
