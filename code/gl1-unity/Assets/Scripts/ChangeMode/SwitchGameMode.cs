﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    public class SwitchGameMode : MonoBehaviour
    {
        public GameObject inventory;
        public GameObject worldMovementControlls;

        private List<GameObject> objectsToDisable = new List<GameObject>();
        private GameObject originConstruction = null;
        private List<GameObject> connections;
        private List<GameObject> elements;

        public void GetWorldSlideReady()
        {
            FindObjectsToSetActiveState(false);
            GetCollidersReady(false);
            inventory.SetActive(false);
            worldMovementControlls.SetActive(false);
        }

        public void GetWorldBuildReady()
        {
            FindObjectsToSetActiveState(true);
            GetCollidersReady(true);
            objectsToDisable.Clear();
            originConstruction = null;
            if (originConstruction != null)
            {
                connections.Clear();
                elements.Clear();
            }
            inventory.SetActive(true);
            worldMovementControlls.SetActive(true);
        }

        private void FindObjectsToSetActiveState(bool state)
        {
            if (!state)
            {
                FindSingleElements();
                FindNonOriginConstructions();
            }
            SetActiveStateOfObjects(state);
        }

        private void FindSingleElements()
        {
            GameObject[] allElements = GameObject.FindGameObjectsWithTag("SlidePart");
            foreach (GameObject obj in allElements)
            {
                if (obj.GetComponent<SnapperComs>().construction == null)
                {
                    objectsToDisable.Add(obj);
                }
            }
        }

        private void FindNonOriginConstructions()
        {
            GameObject[] allElements = GameObject.FindGameObjectsWithTag("Construction");
            foreach (GameObject obj in allElements)
            {
                if (!obj.GetComponent<Construction>().isOrigin)
                {
                    objectsToDisable.Add(obj);
                }
                else
                {
                    originConstruction = obj;
                }
            }
        }

        private void SetActiveStateOfObjects(bool state)
        {
            foreach (GameObject obj in objectsToDisable)
            {
                obj.SetActive(state);
            }
        }

        private void GetCollidersReady(bool state)
        {
            if (originConstruction != null)
            {
                Construction con = originConstruction.GetComponent<Construction>();
                connections = new List<GameObject>(con.connectionList);
                elements = new List<GameObject>(con.elementList);
                SwitchElementCollider(state);
                SwitchConnectionColliders(state);
            }
        }

        private void SwitchElementCollider(bool state)
        {
            foreach (GameObject obj in elements)
            {
                SwitchSnapperObjects(obj, state);
                foreach (Collider collider in obj.GetComponents<Collider>())
                {
                    collider.enabled = state;
                }
                obj.GetComponent<MeshCollider>().enabled = !state;
            }
        }

        private void SwitchSnapperObjects(GameObject element, bool state)
        {
            foreach (Transform transform in element.transform)
            {
                if (transform.tag.Equals("SnapConnector"))
                {
                    transform.gameObject.SetActive(state);
                }
            }
        }

        private void SwitchConnectionColliders(bool state)
        {
            foreach (GameObject obj in connections)
            {
                foreach (Collider collider in obj.GetComponents<Collider>())
                {
                    collider.enabled = state;
                }
            }
        }
    }
}
