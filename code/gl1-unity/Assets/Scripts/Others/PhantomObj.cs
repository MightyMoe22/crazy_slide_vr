﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    public class PhantomObj : MonoBehaviour
    {
        public Material phantomMaterial;

        private bool displayPhantom = false;
        private Transform phantomRef;
        private GameObject phantom;
        private GameObject target;
        private GameObject alignment;

        // Update is called once per frame
        void Update()
        {
            if (displayPhantom)
            {
                //phantom.transform.rotation = phantomRef.rotation;
                Transformer.TransformToTarget(alignment, target, phantom);
            }
        }

        public void StartDisplayPhantom(GameObject targetObj, GameObject moveingObj, int snapperID)
        {
            phantomRef = moveingObj.transform;
            MakePhantom(moveingObj);
            target = targetObj;
            displayPhantom = true;
        }

        public void StopDisplayPhantom()
        {
            Destroy(phantom);
            displayPhantom = false;
        }

        private void MakePhantom(GameObject moveingObj)
        {
            phantom = Instantiate(moveingObj.gameObject);
            phantom.transform.position = moveingObj.transform.position;
            phantom.transform.rotation = moveingObj.transform.rotation;
            phantom.GetComponent<Renderer>().material = phantomMaterial;

        }
    }
}
