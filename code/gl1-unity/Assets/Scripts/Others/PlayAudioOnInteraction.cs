﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    [RequireComponent(typeof(AudioSource))]
    public class PlayAudioOnInteraction : MonoBehaviour
    {
        public AudioClip audioClip;
        public float volume = 1f;
        public void Start()
        {
            GetComponent<AudioSource>().volume = volume;
            GetComponent<AudioSource>().clip = audioClip;
        }
        public void PlaySound()
        {
            GetComponent<AudioSource>().Play();
        }
    }
}
