﻿using System;
using System.Collections;
using UnityEngine;

namespace CrazySlide
{
    [RequireComponent(typeof(SphereCollider))]
    public class KeepPlayerInArea : MonoBehaviour
    {
        public Transform vrPlayArea;

        private Vector3 lastValidPossition;
        private bool coroutineRunning = false;

        private void OnTriggerStay(Collider collision)
        {
            if (collision.gameObject.tag.Equals("HallPlayArea"))
            {
                coroutineRunning = false;
                lastValidPossition = vrPlayArea.position;
            }
        }

        private void OnTriggerExit(Collider collision)
        {
            if (collision.gameObject.tag.Equals("HallPlayArea"))
            {
                coroutineRunning = true;
            }
        }

        private void Update()
        {
            if (coroutineRunning) vrPlayArea.position = lastValidPossition;
        }
    }
}
