﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace CrazySlide
{
    [RequireComponent(typeof(VRTK_InteractableObject))]
    [RequireComponent(typeof(PlayAudioOnInteraction))]
    public class ChangeColorOnInteraction : MonoBehaviour
    {
        public Color colorWhenTouched;
        public Color colorWhenUntouched;

        private GameObject objectToChangeColor;
        private VRTK_InteractableObject vrtkIO;
        private bool setup = false;

        public void SetUpChangeColor(GameObject gameObject)
        {
            objectToChangeColor = gameObject;
            objectToChangeColor.GetComponent<Renderer>().sharedMaterial.color = colorWhenUntouched;
            vrtkIO = GetComponent<VRTK_InteractableObject>();
            vrtkIO.InteractableObjectTouched += InteractableTouched;
            vrtkIO.InteractableObjectUntouched += InteractableUntouched;
            setup = true;
        }
        private void SwapColor(bool touched)
        {
            objectToChangeColor.GetComponent<Renderer>().sharedMaterial.color = touched ? colorWhenTouched : colorWhenUntouched;
        }

        private void OnDisable()
        {
            if (setup) objectToChangeColor.GetComponent<Renderer>().sharedMaterial.color = colorWhenUntouched;
        }

        #region Events
        private void InteractableTouched(object sender, InteractableObjectEventArgs e)
        {
            VRTK_InteractableObject touchedInteractableObject = sender as VRTK_InteractableObject;
            SwapColor(true);
            GetComponent<PlayAudioOnInteraction>().PlaySound();
        }

        private void InteractableUntouched(object sender, InteractableObjectEventArgs e)
        {
            VRTK_InteractableObject untouchedInteractableObject = sender as VRTK_InteractableObject;
            SwapColor(false);
        }
        #endregion Events
    }
}
