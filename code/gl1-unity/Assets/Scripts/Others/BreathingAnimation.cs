﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    public class BreathingAnimation : MonoBehaviour
    {
        [Tooltip("A float value which defines the x and y axis scale")]
        public float elementScale = 0.5f;
        public float scalingSpeed = 0.25f;
        public float scalingFactor = 0.2f;
        void Start()
        {
            StartCoroutine(DisplayAnimation());
        }

        private void Breathing()
        {
            float currentScale = transform.localScale.x;
            float scaleModifier = 1 + Mathf.PingPong(Time.time * scalingSpeed, scalingFactor);
            float nextScale = elementScale * scaleModifier;

            transform.localScale = new Vector3(nextScale, nextScale, transform.localScale.z);
        }

        IEnumerator DisplayAnimation()
        {
            while (true)
            {
                Breathing();
                yield return null;
            }
        }
    }
}
