﻿using UnityEngine;
using VRTK.Controllables;
using VRTK.Controllables.ArtificialBased;

namespace CrazySlide {
    public class LevelLever : MonoBehaviour
    {
        public VRTK_ArtificialRotator artificialRotator;
        public LevelSelectTerminal terminal;

        private float minAngle;

        void Start()
        {
            minAngle = artificialRotator.angleLimits.minimum;
        }

        public void ResetLever()
        {
            artificialRotator.SetValue(minAngle);
        }

        protected virtual void OnEnable()
        {
            artificialRotator = (artificialRotator == null ? GetComponent<VRTK_ArtificialRotator>() : artificialRotator);
            artificialRotator.MaxLimitReached += MaxLimitReached;
            artificialRotator.MinLimitReached += MinLimitReached;
        }

        protected virtual void OnDisable()
        {
            artificialRotator = (artificialRotator == null ? GetComponent<VRTK_ArtificialRotator>() : artificialRotator);
            artificialRotator.MaxLimitReached -= MaxLimitReached;
            artificialRotator.MinLimitReached -= MinLimitReached;
            ResetLever();
        }

        #region Events
        protected virtual void MaxLimitReached(object sender, ControllableEventArgs e)
        {
            terminal.PlayButtonClicked();
        }

        protected virtual void MinLimitReached(object sender, ControllableEventArgs e)
        {
            
        }
        #endregion Events
    }
}
