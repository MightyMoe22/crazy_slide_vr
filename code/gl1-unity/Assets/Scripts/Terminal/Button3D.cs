﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace CrazySlide
{
    public enum Direction
    {
        LEFT, RIGHT, NONE
    }

    public enum ButtonCategory
    {
        SELECT, PLAY, EXIT, YES, NO
    }
    [RequireComponent(typeof(VRTK_InteractableObject))]
    [RequireComponent(typeof(ChangeColorOnInteraction))]
    public class Button3D : MonoBehaviour
    {
        public LevelSelectTerminal terminal;
        public GameObject button;
        public Direction direction;
        public ButtonCategory category;

        private VRTK_InteractableObject vrtkIO;
        private ChangeColorOnInteraction ccoi;

        private void Start()
        {
            vrtkIO = GetComponent<VRTK_InteractableObject>();
            ccoi = GetComponent<ChangeColorOnInteraction>();
            vrtkIO.InteractableObjectUsed += InteractableObjectUsed;
            ccoi.SetUpChangeColor(button);
        }

        private void InteractableObjectUsed(object obj, InteractableObjectEventArgs args)
        {
            switch (category)
            {
                case ButtonCategory.SELECT:
                    terminal.SelectionButtonClicked(direction);
                    break;
                case ButtonCategory.PLAY:
                    terminal.PlayButtonClicked();
                    break;
                case ButtonCategory.EXIT:
                    terminal.ToggleReinsureOptions();
                    break;
                case ButtonCategory.YES:
                    terminal.ExitButtonClicked();
                    break;
                case ButtonCategory.NO:
                    terminal.ToggleReinsureOptions();
                    break;
                default:
                    break;
            }
        }
    }
}
