﻿namespace CrazySlide
{
    public enum LevelName
    {
        TUTORIAL, LEVEL1, LEVEL2, SANDBOX, MAINMENU
    }

    public static class LevelLockState
    {
        //For Demo purpose everything is unlocked
        public const bool MainMenu_Unlocked = true;
        public const bool Tutorial_Unlocked = true;
        public static bool Level_1_Unlocked = true;
        public static bool Level_2_Unlocked = true;
        public static bool Sandbox_Unlocked = true;

        public static void UnlockLevel(LevelName levelName)
        {
            switch (levelName)
            {
                case LevelName.LEVEL1:
                    Level_1_Unlocked = true;
                    break;
                case LevelName.LEVEL2:
                    Level_2_Unlocked = true;
                    break;
                case LevelName.SANDBOX:
                    Sandbox_Unlocked = true;
                    break;
                default:
                    break;
            }
        }

    }
}

