﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrazySlide
{
    [RequireComponent(typeof(SceneLoader))]
    public class LevelSelectTerminal : MonoBehaviour
    {
        public List<GameObject> levels;
        public List<LevelName> levelNames;
        public GameObject lockSymbol;
        public GameObject reinsureButtons;
        public GameObject exitButton;
        public SceneLoader sceneLoader;

        private int currentSelectedLevelID = 0;
        private int nextSelectedLevelID = 0;
        private bool selectionUnlocked = true;
        private bool reinsureButtonsEnabled = false;


        public void SelectionButtonClicked(Direction direction)
        {
            if (direction == Direction.LEFT)
            {
                if (currentSelectedLevelID > 0) nextSelectedLevelID--;
                else nextSelectedLevelID = levels.Count - 1;
            }
            if (direction == Direction.RIGHT)
            {
                if (currentSelectedLevelID < levels.Count - 1) nextSelectedLevelID++;
                else nextSelectedLevelID = 0;
            }
            UpdateSelection();
        }

        public void PlayButtonClicked()
        {
            if (selectionUnlocked)
            {
                sceneLoader.StartLoadingScene(levelNames[currentSelectedLevelID]);
            }
        }

        public void ExitButtonClicked()
        {
            Application.Quit();
        }

        public void ToggleReinsureOptions()
        {
            if (!reinsureButtonsEnabled)
            {
                exitButton.SetActive(reinsureButtonsEnabled);
                reinsureButtonsEnabled = true;
                reinsureButtons.SetActive(reinsureButtonsEnabled);
            }
            else
            {
                exitButton.SetActive(reinsureButtonsEnabled);
                reinsureButtonsEnabled = false;
                reinsureButtons.SetActive(reinsureButtonsEnabled);
            }

        }

        private void UpdateSelection()
        {
            levels[currentSelectedLevelID].SetActive(false);
            levels[nextSelectedLevelID].SetActive(true);
            currentSelectedLevelID = nextSelectedLevelID;
            CheckIfSelectionIsUnlocked();
            UpdateLockDisplay();
        }

        private void CheckIfSelectionIsUnlocked()
        {
            selectionUnlocked = false;
            switch (levelNames[currentSelectedLevelID])
            {
                case LevelName.MAINMENU:
                    selectionUnlocked = LevelLockState.MainMenu_Unlocked;
                    break;
                case LevelName.TUTORIAL:
                    selectionUnlocked = LevelLockState.Tutorial_Unlocked;
                    break;
                case LevelName.LEVEL1:
                    selectionUnlocked = LevelLockState.Level_1_Unlocked;
                    break;
                case LevelName.LEVEL2:
                    selectionUnlocked = LevelLockState.Level_2_Unlocked;
                    break;
                case LevelName.SANDBOX:
                    selectionUnlocked = LevelLockState.Sandbox_Unlocked;
                    break;
                default:
                    break;
            }
        }

        private void UpdateLockDisplay()
        {
            if (!selectionUnlocked)
            {
                lockSymbol.SetActive(true);
            }
            else
            {
                lockSymbol.SetActive(false);
            }
        }

        private void Awake()
        {
            foreach (GameObject go in levels)
            {
                go.SetActive(false);
            }
            levels[currentSelectedLevelID].SetActive(true);
            UpdateSelection();
        }
    }
}
