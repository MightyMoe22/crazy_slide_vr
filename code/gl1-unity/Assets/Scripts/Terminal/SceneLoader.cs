﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CrazySlide
{
    public class SceneLoader : MonoBehaviour
    {
        public void StartLoadingScene(LevelName levelName)
        {
            int sceneIndex = -1;
            switch (levelName)
            {
                case LevelName.MAINMENU:
                    sceneIndex = 0;
                    break;
                case LevelName.TUTORIAL:
                    sceneIndex = 1;
                    break;
                case LevelName.LEVEL1:
                    sceneIndex = 2;
                    break;
                case LevelName.LEVEL2:
                    sceneIndex = 3;
                    break;
                case LevelName.SANDBOX:
                    sceneIndex = 4;
                    break;
                default:
                    break;
            }
            StartCoroutine(LoadSceneInAsync(sceneIndex));
        }

        IEnumerator LoadSceneInAsync(int sceneIndex)
        {
            Debug.Log("Loading Scene at Index: " + sceneIndex);
            if (sceneIndex > -1)
            {
                AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneIndex);

                // Wait until the asynchronous scene fully loads
                while (!asyncLoad.isDone)
                {
                    yield return null;
                }
            } 
        }
    }
}
